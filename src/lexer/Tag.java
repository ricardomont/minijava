package lexer;

public class Tag {
    public final static int INT = 251;
    public final static int BOOL = 252;
    public final static int STRING = 253;
    public final static int CLASS = 254;
    public final static int STATIC = 255;
    public final static int VOID = 256;
    public final static int PUBLIC = 257;

    public final static int EXTENDS = 258;
    public final static int RETURN = 259;

    public final static int IF = 260;
    public final static int ELSE = 261;
    public final static int WHILE = 262;
    public final static int MAIN = 263;
    public final static int SOP = 264;
    public final static int LENGTH = 265;

    public final static int TRUE = 266;
    public final static int FALSE = 267;
    public final static int NULL = 268;

    public final static int THIS = 269;
    public final static int NEW = 270;

    public final static int NUM = 271;
    public final static int ID = 272;

    public final static int AND = 273;
    public final static int NOT = 274;

    public final static int EQUAL = 275;
    public final static int NEQ = 276;
    public final static int GEQ = 277;
    public final static int LEQ = 278;
    public final static int GREATER = 279;
    public final static int LESS = 280;

    public final static int ADD = 281;
    public final static int SUB = 282;
    public final static int MUL = 283;

    public final static int ASS = 284;

    public final static int BASIC = 285;
    public final static int ARRAY = 286;
}
