package lexer;

public class Numero extends Token {
    public static int num;

    public Numero(int num){
        super(Tag.NUM);
        this.num = num;
    }
}
