package lexer;

public class Palavra extends Token {
    public final String lexema;

    public Palavra(String lexema, int tag){
        super(tag);
        this.lexema = lexema;
    }

    public static final Palavra BOOL = new Palavra("boolean", Tag.BOOL),
            CLASS = new Palavra("class", Tag.CLASS),
            EXTENDS = new Palavra("extends", Tag.EXTENDS),
            PUBLIC = new Palavra("public", Tag.PUBLIC),
            STATIC = new Palavra("static", Tag.STATIC),
            VOID = new Palavra("void", Tag.VOID),
            MAIN = new Palavra("main", Tag.MAIN),
            STRING = new Palavra("String", Tag.STRING),
            RETURN = new Palavra("return", Tag.RETURN),
            INT = new Palavra("int", Tag.INT),
            IF = new Palavra("if", Tag.IF),
            ELSE = new Palavra("else", Tag.ELSE),
            WHILE = new Palavra("while", Tag.WHILE),
            SOP = new Palavra("System.out.println", Tag.SOP),
            LENGTH = new Palavra("length", Tag.LENGTH),
            TRUE = new Palavra("true", Tag.TRUE),
            FALSE = new Palavra("false", Tag.FALSE),
            THIS = new Palavra("this", Tag.THIS),
            NEW = new Palavra("new", Tag.NEW),
            NULL = new Palavra("null", Tag.NULL),
            AND = new Palavra("&&", Tag.AND),
            NOT = new Palavra("!", Tag.NOT),
            EQUAL = new Palavra("==", Tag.EQUAL),
            NEQ = new Palavra("!=", Tag.NEQ),
            GEQ = new Palavra(">=", Tag.GEQ),
            LEQ = new Palavra("<=", Tag.LEQ),
            GREATER = new Palavra(">", Tag.GREATER),
            LESS = new Palavra("<", Tag.LESS),
            ADD = new Palavra("+", Tag.ADD),
            SUB = new Palavra("-", Tag.SUB),
            MUL = new Palavra("*", Tag.MUL),
            ASS = new Palavra("=", Tag.ASS);
}
