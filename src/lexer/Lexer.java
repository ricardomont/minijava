package lexer;

import java.io.IOException;
import java.util.Hashtable;

public class Lexer {
    private Hashtable<String, Palavra> palavras = new Hashtable<>();
    private char pegar = ' ';
    public static int linha = 1;

    private void reservar(Palavra pal){
        palavras.put(pal.lexema, pal);
    }

    private void palavrasReservadas(){
        this.reservar(Palavra.BOOL);
        this.reservar(Palavra.CLASS);
        this.reservar(Palavra.ELSE);
        this.reservar(Palavra.EXTENDS);
        this.reservar(Palavra.FALSE);
        this.reservar(Palavra.IF);
        this.reservar(Palavra.INT);
        this.reservar(Palavra.LENGTH);
        this.reservar(Palavra.MAIN);
        this.reservar(Palavra.NEW);
        this.reservar(Palavra.NULL);
        this.reservar(Palavra.PUBLIC);
        this.reservar(Palavra.RETURN);
        this.reservar(Palavra.SOP);
        this.reservar(Palavra.STATIC);
        this.reservar(Palavra.STRING);
        this.reservar(Palavra.THIS);
        this.reservar(Palavra.TRUE);
        this.reservar(Palavra.VOID);
        this.reservar(Palavra.WHILE);
    }

    public Lexer(){
        this.palavrasReservadas();
    }

    private void ler() throws IOException{
        this.pegar = (char) System.in.read();
    }

    private boolean ler(char c) throws IOException{
        ler();
        if(this.pegar != c){
            return false;
        }
        this.pegar = ' ';
        return true;
    }

    public Token scan() throws IOException{
        for(;;this.ler()){
            if((this.pegar == ' ') || (this.pegar == '\t')){

            }else if(this.pegar == '\n'){
                this.linha++;
            }else{
                break;
            }
        }

        switch (this.pegar){
            case '&':
                if(this.ler('&')){
                    return Palavra.AND;
                }else{
                    System.err.println("& nao esta definido");
                }
                break;

            case '!':
                if(this.ler('=')){
                    return Palavra.NEQ;
                }else{
                    return Palavra.NOT;
                }

            case '=':
                if(this.ler('=')){
                    return Palavra.EQUAL;
                }else{
                    return Palavra.ASS;
                }

            case '<':
                if(this.ler('=')){
                    return Palavra.LEQ;
                }else{
                    return Palavra.LESS;
                }

            case '>':
                if(this.ler('=')){
                    return Palavra.GEQ;
                }else{
                    return Palavra.GREATER;
                }

            case '+':   return Palavra.ADD;
            case '-':   return Palavra.SUB;
            case '*':   return Palavra.MUL;
        }

        if(Character.isDigit(this.pegar)) {
            int valor = 0;
            do {
                valor = 10 * valor * Character.digit(this.pegar, 10);
                this.ler();
            } while (Character.isDigit(this.pegar));
            return new Numero(valor);
        }

        if(Character.isLetter(this.pegar)){
            StringBuilder b = new StringBuilder();
            do{
                b.append(this.pegar);
                this.ler();
            }while (Character.isLetterOrDigit(this.pegar));

            String s = b.toString();

            Palavra pal = this.palavras.get(s);

            if(pal != null){
                return pal;
            }

            return new Palavra(s, Tag.ID);
        }

        Token t = new Token(this.pegar);
        this.pegar = ' ';
        return t;
    }
}
