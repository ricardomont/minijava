package simbolos;

import lexer.Palavra;
import lexer.Tag;

public class Tipo extends Palavra {
    public final int largura;

    public Tipo(String lexema, int tag, int largura){
        super(lexema, tag);
        this.largura = largura;
    }

    public static Tipo INT = new Tipo("int", Tag.BASIC, 4);
    public static Tipo CHAR = new Tipo("char", Tag.BASIC, 1);
    public static Tipo BOOLEAN = new Tipo("boolean", Tag.BASIC, 1);

    public static boolean isNumerico(Tipo p) {
        return (p == Tipo.CHAR) || (p == Tipo.INT);
    }

    public static Tipo maxNumericoTipo(Tipo t1, Tipo t2) {
        if (!isNumerico(t1) || !isNumerico(t2)) {
            return null;
        }
        if (t1 == Tipo.INT || t2 == Tipo.INT) {
            return Tipo.INT;
        }
        return Tipo.CHAR;
    }
}
