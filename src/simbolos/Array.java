package simbolos;

import lexer.Tag;

public class Array extends Tipo {
    public final Tipo de;
    public final int tamanho;

    public Array(int tamanho, Tipo de){
        super("[]", Tag.ARRAY, (tamanho * de.largura));
        this.de = de;
        this.tamanho = tamanho;
    }
}
